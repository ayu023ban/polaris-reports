import { makeStyles } from "@material-ui/core";
import React, { useEffect, useState } from "react";
import Chart from "react-apexcharts";

const useStyles = makeStyles((theme) => ({
  container: { background: theme.palette.background.default },
}));

const getTimeStamps = (timestrings) =>
  timestrings.map((time) => {
    let temp = new Date(time);
    return temp.getTime();
  });

const parseData = (timestamps, jsonData) => {
  const columns = Object.keys(jsonData.data.values);
  const newData = [
    {
      name: "positive",
      data: [],
    },
    { name: "negative", data: [] },
  ];
  for (let i = 0; i < timestamps.length; i++) {
    let pos = 0;
    let neg = 0;
    columns.forEach((col) => {
      let temp = jsonData.data.values[col]["individual_values"][i];
      if (temp > 0) {
        pos += temp;
      } else {
        neg += temp;
      }
    });
    newData[0].data.push({ x: timestamps[i], y: pos.toFixed(2) });
    newData[1].data.push({ x: timestamps[i], y: neg.toFixed(2) });
  }
  return newData;
};

const getOptions = () => ({
  chart: {
    height: 280,
    type: "area",
    foreColor: "#ccc",
    zoom: {
      autoScaleYaxis: true,
    },
    toolbar: {
      theme: "dark",
      autoSelected: "pan",
    },
  },
  colors: ["#00BAEC"],
  stroke: {
    width: 3,
  },
  grid: {
    borderColor: "#555",
    clipMarkers: false,
    yaxis: {
      lines: {
        show: false,
      },
    },
  },
  tooltip: {
    theme: "dark",
  },
  dataLabels: {
    enabled: false,
  },
  fill: {
    type: "gradient",
    gradient: {
      enabled: true,
      opacityFrom: 0,
      opacityTo: 1,
    },
  },
  legend: {
    show: false,
  },
  xaxis: {
    type: "datetime",
  },
});

const getXAxisAnnotations = (json_data, timestamps) =>
  json_data.data.events.map((event) => {
    return {
      x: timestamps[event],
      strokeDashArray: 0,
      borderColor: "#FF0080",
    };
  });

const StackedGraph = ({ jsonData }) => {
  const [options, setOptions] = useState(getOptions());
  const [data, setData] = useState([
    {
      name: "positive",
      data: [],
    },
    { name: "negative", data: [] },
  ]);
  const classes = useStyles();

  useEffect(() => {
    let timestamps = [];
    if (jsonData.data && jsonData.data.timestamps) {
      timestamps = getTimeStamps(jsonData.data.timestamps);
    }
    if (jsonData.data) {
      const newData = parseData(timestamps, jsonData);
      setData(newData);

      const newOptions = {
        ...getOptions(),
        annotations: {
          xaxis: getXAxisAnnotations(jsonData, timestamps),
        },
      };
      setOptions(newOptions);
    }
  }, [jsonData, jsonData.data]);

  return (
    <div className={`mixed-chart ${classes.container}`}>
      <Chart options={options} series={data} type="area" width="1000" />
    </div>
  );
};

export default StackedGraph;
