const themes = {
  light: {
    type: "light",
    primary: { main: "#f0f2f5", contrastText: "#000000" },
    secondary: { main: "#356fff", contrastText: "#ffffff" },
    background: { default: "#f0f2f5", paper: "#ffffff" },
  },
  dark: {
    type: "dark",
    primary: { main: "#282828", contrastText: "#ffffff" },
    secondary: { main: "#356fff", contrastText: "#ffffff" },
    background: { default: "#000524", paper: "#242526" },
    custom: { radient: "#00FFF0" },
  },
};

export default themes;
